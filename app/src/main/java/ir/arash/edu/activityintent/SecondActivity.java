package ir.arash.edu.activityintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    //---------------------------------------//
    //  Dev By Arash Azizi                   //
    //  Gitlab : https://gitlab.com/SirArash //
    //---------------------------------------//


    Button btn_webpage;
    Button btn_sms;
    Button btn_call;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViews();
    }

    public void initViews(){
        btn_webpage = findViewById(R.id.btn_webpage);
        btn_sms = findViewById(R.id.btn_sms);
        btn_call = findViewById(R.id.btn_call);
        btn_back = findViewById(R.id.btn_back);

        btn_webpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent BrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/SirArash"));
                startActivity(BrowserIntent);
            }
        });

        btn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNum = "09380000000";
                Uri smsURI = Uri.fromParts("sms",phoneNum,null);
                Intent smsIntent = new Intent(Intent.ACTION_VIEW,smsURI);
                startActivity(smsIntent);
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNum = "09380000001";
                Uri callURI = Uri.parse("tel:" + phoneNum);
                Intent callIntent = new Intent(Intent.ACTION_DIAL,callURI);
                startActivity(callIntent);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}